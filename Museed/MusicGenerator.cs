﻿using System;
using System.Collections.Generic;
using Music.Models;

namespace Museed
{
    public class MusicGenerator
    {
        private static readonly Random random = new Random();
        public Song GenerateTopTenHit(int length)
        {
            var notes = RandomNotes(length);
            var song = new Song(notes, random.Next(50, 200));
            return song;
        }

        private static List<Note> RandomNotes(int length)
        {
            var notes = new List<Note>();
            for (var i = 0; i < length; i++)
            {
                notes.Add(RandomNote(i));
            }
            return notes;
        }

        private static Note RandomNote(int position)
        {
            return new Note(random.Next(0, 12), RandomNoteValue(), position);
        }

        private static NoteValue RandomNoteValue()
        {
            return new NoteValue(random.Next(1, 7), random.Next(0, 3) == 0);
        }
    }
}
