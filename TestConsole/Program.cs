﻿using System;
using Conversion;
using Museed;

namespace TestConsole
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine(FakeSong(20));
            Console.ReadLine();
        }

        private static string FakeSong(int length)
        {
            return new SongToStringConverter()
                .Convert(
                    new MusicGenerator()
                        .GenerateTopTenHit(length));
        }
    }
}