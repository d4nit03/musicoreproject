﻿﻿namespace Conversion
{
    public interface IConverter<TInputType, TOutputType>
    {
        TOutputType Convert(TInputType input);
        TInputType ConvertBack(TOutputType input);
    }
}
