﻿using Music.Models;
using static Conversion.Prefixes;

namespace Conversion
{
    internal class NoteToStringConverter : IConverter<Note, string>
    {
        NoteValueToStringConverter noteValueToStringConverter = new NoteValueToStringConverter();
        public string Convert(Note input)
        {
            var noteString = PitchPrefix
                             + input.Pitch
                             + NoteValuePrefix
                             + OpenSubElement
                             + noteValueToStringConverter.Convert(input.NoteValue)
                             + CloseSubElement
                             + PositionPrefix
                             + input.Position;
            return noteString;
        }

        public Note ConvertBack(string input)
        {
            throw new System.NotImplementedException();
        }
    }
}
