﻿using Music;
using Music.Models;
using static Conversion.Prefixes;

namespace Conversion
{
    internal class NoteValueToStringConverter : IConverter<NoteValue, string>
    {
        public string Convert(NoteValue input)
        {
            return "" + IdValuePrefix
                      + input.IdValue
                      + BeamPrefix
                      + (input.Beam ? '1' : '0')
                      + TupletPrefix
                      + input.Tuplet;
        }

        public NoteValue ConvertBack(string input)
        {
            throw new System.NotImplementedException();
        }
    }
}