﻿using Music.Models;
using static Conversion.Prefixes;

namespace Conversion
{
    public class SongToStringConverter: IConverter<Song, string>
    {
        private NoteToStringConverter noteToStringConverter = new NoteToStringConverter();
        public string Convert(Song input)
        {
            var songString = "";
            songString += SongPrefix;
            foreach (var inputNote in input.Notes)
            {
                songString += NotePrefix
                              + OpenSubElement
                              + noteToStringConverter.Convert(inputNote)
                              + CloseSubElement;
            }

            songString += BpmPrefix + input.Bpm;
            return songString;
        }

        public Song ConvertBack(string input)
        {
            throw new System.NotImplementedException();
        }
    }
}