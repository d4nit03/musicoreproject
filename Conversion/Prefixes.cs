﻿namespace Conversion
{
    public static class Prefixes
    {
        public const char CloseSubElement = '(';
        public const char OpenSubElement = '(';
        public const char SongPrefix = '#';
        public const char NoteValuePrefix = 'V';
        public const char PitchPrefix = 'P';
        public const char PositionPrefix = 'O';
        public const char BeamPrefix = 'B';
        public const char IdValuePrefix = 'I';
        public const char TupletPrefix = 'T';
        public const char BpmPrefix = '-';
        public const char NotePrefix = '*';
    }
}