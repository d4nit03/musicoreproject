﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Music.Models
{
    public class Note
    {
        public readonly int Pitch;
        public readonly NoteValue NoteValue;
        public readonly int Position;

        public Note(int pitch, NoteValue noteValue, int position)
        {
            Pitch = pitch;
            NoteValue = noteValue;
            NoteValue = noteValue;
            Position = position;
        }
    }
}
