﻿namespace Music.Models
{
    public class NoteValue
    {
        public readonly int IdValue;
        public readonly bool Beam;
        public readonly int Tuplet;

        public NoteValue(int idValue, bool beam, int tuplet = 0)
        {
            IdValue = idValue;
            Beam = beam;
            Tuplet = tuplet;
        }
    }
}