﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Music.Models
{
    public class Song
    {
        public readonly List<Note> Notes;
        public readonly double Bpm;

        public Song(List<Note> notes, double bpm)
        {
            Notes = notes;
            Bpm = bpm;
        }
    }
}
